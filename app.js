"use strict"


//Menu functions.
//Used for the overall flow of the application.
/////////////////////////////////////////////////////////////////
//#region 
// app is the function called to start the entire application
function app(people){
  let searchType = promptFor("Do you know the name of the person you are looking for? Enter 'yes' or 'no'", yesNo).toLowerCase();
  let searchResults;
  switch(searchType){
    case 'yes':
      searchResults = searchByName(people);
      mainMenu(searchResults, people);
      break;
    case 'no':
      searchResults = searchByTraits(people);
      break;
    default:
      app(people); // restart app
      break;
  }
}

// Menu function to call once you find who you are looking for
function mainMenu(person, people){

  /* Here we pass in the entire person object that we found in our search, as well as the entire original dataset of people. We need people in order to find descendants and other information that the user may want. */
  if(!person){
    alert("Could not find that individual.");
    return app(people); // restart
  }
  let displayOption = promptFor("Found " + person.firstName + " " + person.lastName + " . Do you want to know their 'info', 'family', or 'descendants'? Type the option you want or 'restart' or 'quit'", personSearchValid).toLowerCase();

  switch(displayOption){
    case "info":
      displayPerson(person);
      break;
    case "family":
      displayFamily(person, people);
      break;
    case "descendants":
      //implementation of descendants that doesn't use recursion
      //displayDescendants(person, people);

      //recursive implementation of displayDescendants
      let descendants = getDescendantsRecur(person, people);
      displayFoundDescendants(person, descendants);
      break;
    case "restart":
      app(people); // restart
      break;
    case "quit":
      return; // stop execution
    default:
      return mainMenu(person, people); // ask again
  }
}

//#endregion
//Filter functions.
//Ideally you will have a function for each trait.
/////////////////////////////////////////////////////////////////
//#region 

//nearly finished function used to search through an array of people to find matching first and last name and return a SINGLE person object.
function searchByName(people){
  let firstName = promptFor("What is the person's first name?", nameValid);
  let lastName = promptFor("What is the person's last name?", nameValid);
  //filter the array of people to find the listing that has a matching last and first name from the input
  let foundPerson = people.filter(function(potentialMatch){
    if(potentialMatch.firstName === firstName && potentialMatch.lastName === lastName){
      return true;
    }
    else{
      return false;
    }
  });
  if(foundPerson.length == 1){
    return foundPerson[0];
  }else{
    return undefined;
  }
}

function searchByTraits(people){
  let peopleFound;
  let peopleToSearch = people;
  let numTraits = promptFor("Please enter the number of traits you would like to filter by? (Maximum of 5)", numTraitsValid);
  let filterSelectMessage = "Please enter which trait you would like to filter by using the following.\n" + 
    "1 = Gender, 2 = Date of Birth, 3 = Height, 4 = Weight, 5 = Eye Color, 6 = Occupation";
  for(let i = numTraits; i > 0; i--){
    let filterSelect = promptFor(filterSelectMessage, filterSelectValid);
    switch(parseInt(filterSelect)){
      case 1:
        peopleFound = searchByGender(peopleToSearch);
        peopleToSearch = peopleFound;
        displayPeople(peopleFound);
        break;
      case 2:
        peopleFound = searchByBirthDate(peopleToSearch);
        peopleToSearch = peopleFound;
        displayPeople(peopleFound);
        break;
      case 3:
        peopleFound = searchByHeight(peopleToSearch);
        peopleToSearch = peopleFound;
        displayPeople(peopleFound);
        break;
      case 4:
        peopleFound = searchByWeight(peopleToSearch);
        peopleToSearch = peopleFound;
        displayPeople(peopleFound);
        break;
      case 5:
        peopleFound = searchByEyeColor(peopleToSearch);
        peopleToSearch = peopleFound;
        displayPeople(peopleFound);
        break;
      case 6:
        peopleFound = searchByOccupation(peopleToSearch);
        peopleToSearch = peopleFound;
        displayPeople(peopleFound);
        break;
    }
  }
}

//unfinished function to search through an array of people to find matching eye colors. Use searchByName as reference.
function searchByEyeColor(people){
  let eyeColorVal = promptFor("Please enter they eye color of people you would like to filter by.", eyeColorValid).toLowerCase();
  let peopleWithEyeColor = people.filter(function(possPerson){
    return possPerson.eyeColor === eyeColorVal;
  });

  return peopleWithEyeColor;
}

function searchByOccupation(people){
  let occupationVal = promptFor("Please enter the occupation of people you would like to filter by.", occupationValid).toLowerCase();
  let peopleWithOccupation = people.filter(function(possPerson){
    return possPerson.occupation === occupationVal;
  });
  
  return peopleWithOccupation;
}

function searchByWeight(people){
  let weightSearchVal = promptFor("Please enter the weight of people you would like to filter by.", numValid);
  let peopleWithWeight = people.filter(function(possPerson){
    return possPerson.weight === parseInt(weightSearchVal);
  });
  
  return peopleWithWeight;
}

function searchByHeight(people){
  let heightSearchVal = promptFor("Please enter the height of people you would like to filter by.", numValid);
  let peopleWithHeight = people.filter(function(possPerson){
    return possPerson.height === parseInt(heightSearchVal);
  });

  return peopleWithHeight;
}

function searchByGender(people){
  let genderSearchVal = promptFor("Please enter the gender that you would like to filter by.", genderValid).toLowerCase();
  let peopleWithGender = people.filter(function(possPerson){
    return possPerson.gender === genderSearchVal;
  });

  return peopleWithGender;
}

function searchByBirthDate(people){
  let birthDateVal = promptFor("Please enter the date of birth that you would like to filter by. Use the MM/DD/YYYY format.", birthDateValid);
  let birthDateParts = birthDateVal.split("/");
  let month = parseInt(birthDateParts[0], 10);
  let day = parseInt(birthDateParts[1], 10);
  let birthDate = month + "/" + day + "/" + birthDateParts[2];
  let birthDateWithZero = month + "/" + birthDateParts[1] + "/" + birthDateParts[2];

  let peopleWithBirthDate = people.filter(function(possPerson){
    return (possPerson.dob === birthDate || possPerson.dob === birthDateWithZero);
  });

  return peopleWithBirthDate;
}

//#endregion

//Display functions.
//Functions for user interface.
/////////////////////////////////////////////////////////////////
//#region 

// alerts a list of people
function displayPeople(people){
  alert(people.map(function(person){
    return person.firstName + " " + person.lastName;
  }).join("\n"));
}

function displayPerson(person){
  // print all of the information about a person:
  // height, weight, age, name, occupation, eye color.
  let personInfo = "First Name: " + person.firstName + "\n";
  personInfo += "Last Name: " + person.lastName + "\n";
  personInfo += "Date of Birth: " + person.dob + "\n";
  personInfo += "Gender: " + person.gender + "\n";
  personInfo += "Height: " + person.height + "\n";
  personInfo += "Weight: " + person.weight + "\n";
  personInfo += "Eye color: " + person.eyeColor + "\n";
  personInfo += "Occupation: " + person.occupation + "\n";
  alert(personInfo);
}

//function to display the family information about a specific person
function displayFamily(person, people){
  let personName = person.firstName + " " + person.lastName;
  let header = personName + "'s Family Information\n\n";
  let parents = "Parents\n";
  let siblings = "Siblings\n";
  let spouse = "Spouse\n";

  if(person.parents.length === 0){
    siblings += personName + " has no known siblings.\n\n";
    parents += personName + " has no known parents.\n\n";
  }else{
    siblings += findSiblings(person, people);
    parents += findParents(person, people);
  }

  if(person.currentSpouse === null){
    spouse += personName +  " does not have a spouse.";
  }else{
    spouse += findSpouse(person, people);
  }
  alert(header + parents + siblings + spouse);
}

function findSpouse(person, people){
  let spouse = people.filter(function(possSpouse){
    if(this.currentSpouse === possSpouse.id){
      return true;
    }else{
      return false;
    }
  }, person);
  let relation = (spouse[0].gender === "male") ? "Husband" : "Wife";
  return "Name: " + spouse[0].firstName + " " + spouse[0].lastName + " " + "Relationship: " + relation;
} 

function findSiblings(person, people){
  let displayString = "";
  let siblings = people.filter(function(possSibling){
    let parentsCompare = this.parents;
    return parentsCompare.length == possSibling.parents.length &&
      parentsCompare.every((val, index) => val === possSibling.parents[index]) &&
      possSibling.firstName !== this.firstName;
  }, person);

  for(let i = 0; i < siblings.length; i++){
    let relation = (siblings[i].gender === "male") ? "Brother" : "Sister";
    displayString += "Name: " + siblings[i].firstName + " " + siblings[i].lastName + 
      " Relationship: " + relation + "\n";
  }

  return displayString + "\n";
}

function findParents(person, people){
  let parents = [];
  let compareParents = person.parents;
  let displayString = "";

  compareParents.forEach(parent => {
    let personParent = people.filter(function(possParent){
      if(parent === possParent.id){
        return true;
      }else{
        return false;
      }
    });
    parents.push(personParent[0]);
  });

  parents.forEach(parent => {
    let relation = (parent.gender === "male") ? "Father" : "Mother";
    displayString += "Name: " + parent.firstName + " " + parent.lastName + 
      " Relationship: " + relation + "\n";
  });

  return displayString + "\n";
}

function displayDescendants(person, people){
  let personName = person.firstName + " " + person.lastName;
  let header = personName + "'s Descendants\n\n"; 
  let displayString = "";
  let descendants = people.filter(function(possDescendant){
    return (possDescendant.parents.includes(this.id)) ? true : false;
  }, person);

  if(descendants.length === 0){
    displayString = personName + " does not have any descendants.";
  }else{
    descendants.forEach(descendant => {
      displayString += "Name: " + descendant.firstName + " " + descendant.lastName + "\n";
    });
  }

  alert(header + displayString);
}

function getDescendantsRecur(person, people, currDescendants = [], index=0){
  if(index >= people.length){
    return currDescendants;
  }

  let personToCompare = people[index];
  index++;
  if(personToCompare.parents.includes(person.id)){
    currDescendants.push(personToCompare);
    return getDescendantsRecur(person, people, currDescendants, index);
  }else{
    return getDescendantsRecur(person, people, currDescendants, index);
  }
}

function displayFoundDescendants(person, descendants){
  let displayString = "";
  let personName = person.firstName + " " + person.lastName;
  let header = personName + "'s Descendants\n\n"; 

  if(descendants.length === 0){
    displayString = personName + " does not have any descendants.";
  }else{
    descendants.forEach(descendant => {
      displayString += "Name: " + descendant.firstName + " " + descendant.lastName + "\n";
    });
  }

  alert(header + displayString);
}
//#endregion



//Validation functions.
//Functions to validate user input.
/////////////////////////////////////////////////////////////////
//#region 

//a function that takes in a question to prompt, and a callback function to validate the user input.
//response: Will capture the user input.
//isValid: Will capture the return of the validation function callback. true(the user input is valid)/false(the user input was not valid).
//this function will continue to loop until the user enters something that is not an empty string("") or is considered valid based off the callback function(valid).
function promptFor(question, valid){
  let isValid;
  do{
    var response = prompt(question).trim();
    isValid = valid(response);
  } while(response === ""  ||  isValid === false)
  return response;
}

//helper function/callback to pass into promptFor to validate yes/no answers.
function yesNo(input){
  if(input.toLowerCase() == "yes" || input.toLowerCase() == "no"){
    return true;
  }
  else{
    return false;
  }
}

// helper function to pass in as default promptFor validation.
//this will always return true for all inputs.
function autoValid(input){
  return true; // default validation only
}

function personSearchValid(input){
  switch(input.toLowerCase()){
    case "info":
    case "family":
    case "descendants":
    case "restart":
    case "quit":
      return true;
    default:
      return false;
  }
}

function occupationValid(input){
  return !(/\d/.test(input));
}

function numValid(input){
  return !isNaN(input);
}

function eyeColorValid(input){
  switch(input.toLowerCase()){
    case "brown":
    case "black":
    case "blue":
    case "hazel":
    case "green":
      return true;
    default: 
      return false;
  }
}

function birthDateValid(input){
  if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(input)){
    return false; 
  } 
  
  let dateParts = input.split("/");
  let month = parseInt(dateParts[0], 10);
  let day = parseInt(dateParts[1], 10);
  let year = parseInt(dateParts[2], 10);

  if(year < 1000 || year > 3000 || month == 0 || month > 12){
    return false;
  }

  var monthLengths = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

  //adjust February in case of a leap year
  if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)){
    monthLengths[1] = 29;
  }

  return day > 0 && day <= monthLengths[month - 1];
}

function genderValid(input){
  if(!isNaN(input)){
    return false;
  }
  let gender = input.toLowerCase();
  if(gender === "male" || gender === "female"){
    return true;
  }else{
    return false;
  }
}

function filterSelectValid(input){
  let filterSelectNum = parseInt(input);
  if(filterSelectNum >= 1 && filterSelectNum <= 8){
    return true;
  }else{
    return false;
  }
}

//function to check that the number of traits to search by is a valid number between 1 and 5
function numTraitsValid(input){
  let numTraits = parseInt(input);
  if(numTraits >= 1 && numTraits <= 5){
    return true;
  }else{
    return false;    
  }
}

//function that will validate whether a name that is input is valid or not
//a name is valid if it is not a number and the first letter is capitalized
function nameValid(name){
  if(isNaN(name) && isCapital(name.charAt(0))){
    return true;
  }else{
    return false;
  }
}

//function used for name validation to check whether the first letter in the name is capitalized
function isCapital(char){
  if(char === char.toUpperCase()){
    return true;
  }else{
    return false;
  } 
}

//#endregion